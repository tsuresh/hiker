package com.hiker.hiker;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class splash extends AppCompatActivity {

    RelativeLayout rellay1, rellay2;

    private EditText Name;
    private EditText Password;
    private Button Login;
    private int count = 3;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            rellay1.setVisibility(View.VISIBLE);
            rellay2.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        rellay1 = (RelativeLayout) findViewById(R.id.rellay1);
        rellay2 = (RelativeLayout) findViewById(R.id.rellay2);

        handler.postDelayed(runnable, 2000); // Splash Screen Timeout

        //------------------------------------------------------------------------
        Name = (EditText) findViewById(R.id.etName);
        Password = (EditText) findViewById(R.id.etPassword);
        Login = (Button) findViewById(R.id.btnLogin);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(Name.getText().toString(), Password.getText().toString());
            }
        });
    }

    private void validate(String userName, String userPassword){
        if((userName.equals("vidura")) && (userPassword.equals("hello"))){
            Intent intent = new Intent(splash.this, dashboard.class);
            Toast.makeText(getApplicationContext(),"Login Successfully",Toast.LENGTH_LONG).show();
            startActivity(intent);
        }
        else{
            count--;
            Toast.makeText(getApplicationContext(),"Incorrect Username or Password",Toast.LENGTH_LONG).show();
            if(count == 0){
                Login.setEnabled(false);
                Toast.makeText(getApplicationContext(),"Too many incorrect attempts",Toast.LENGTH_LONG).show();
            }
        }
    }
}

